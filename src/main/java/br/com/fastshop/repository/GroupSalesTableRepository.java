package br.com.fastshop.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.fastshop.entity.GroupSalesTable;

@Repository
public interface GroupSalesTableRepository extends MongoRepository<GroupSalesTable, String> {

	GroupSalesTable findByName(String name);
}
