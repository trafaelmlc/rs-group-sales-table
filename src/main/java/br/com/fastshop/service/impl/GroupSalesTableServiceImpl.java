package br.com.fastshop.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.fastshop.entity.GroupSalesTable;
import br.com.fastshop.repository.GroupSalesTableRepository;
import br.com.fastshop.service.GroupSalesTableService;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class GroupSalesTableServiceImpl implements GroupSalesTableService {

	@Autowired
	private GroupSalesTableRepository repository;

	@Override
	public ResponseEntity<GroupSalesTable> create(GroupSalesTable salesTable) {
		log.info("insert salesTable type");
		try {
			return ResponseEntity.ok(repository.insert(salesTable));
		} catch (Exception me) {
			if (me instanceof DuplicateKeyException) {
				return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
			} else {
				return null;
			}
		}
	}

	@Override
	public ResponseEntity<GroupSalesTable> update(GroupSalesTable salesTable) {
		log.info("update salesTable type");
		try {
			return ResponseEntity.ok(repository.save(salesTable));
		} catch (Exception me) {
			if (me instanceof DuplicateKeyException) {
				return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
			} else {
				return null;
			}
		}
	}

	@Override
	public ResponseEntity<GroupSalesTable> getById(String id) {
		log.info("get salesTable type by id");
		Optional<GroupSalesTable> salesTable = repository.findById(id);
		if (salesTable.isPresent()) {
			return ResponseEntity.ok(salesTable.get());
		} else {
			return null;
		}
	}

	@Override
	public ResponseEntity<List<GroupSalesTable>> getAll() {
		log.info("get all salesTable type");
		return ResponseEntity.ok(repository.findAll());
	}

	@Override
	public Boolean deleteById(String id) {
		log.info("delete salesTable type by id");
		Boolean delete = getById(id) != null ? true : false;
		try {
			if (delete) {
				repository.deleteById(id);
			}
		} catch (Exception e) {
			log.error("Error: " + e.getMessage());
		}
		return delete;
	}

	@Override
	public ResponseEntity<GroupSalesTable> findByName(String name) {
		log.info("find by name salesTable type");
		return ResponseEntity.ok(repository.findByName(name));
	}
}
