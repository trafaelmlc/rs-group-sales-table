package br.com.fastshop.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import br.com.fastshop.entity.GroupSalesTable;

public interface GroupSalesTableService {

	ResponseEntity<GroupSalesTable> create(GroupSalesTable grupo);

	ResponseEntity<GroupSalesTable> update(GroupSalesTable grupo);

	ResponseEntity<GroupSalesTable> getById(String id);

	ResponseEntity<GroupSalesTable> findByName(String name);

	ResponseEntity<List<GroupSalesTable>> getAll();

	Boolean deleteById(String id);

}
