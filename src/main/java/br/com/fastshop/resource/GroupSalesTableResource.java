package br.com.fastshop.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.fastshop.entity.GroupSalesTable;
import br.com.fastshop.service.GroupSalesTableService;

@RestController
@RequestMapping("/api")
public class GroupSalesTableResource {

	@Autowired
	private GroupSalesTableService service;

	@PostMapping("/")
	public ResponseEntity<GroupSalesTable> insert(@RequestBody GroupSalesTable groupSalesTable) {
		return service.create(groupSalesTable);
	}

	@PutMapping("/")
	public ResponseEntity<GroupSalesTable> update(@RequestBody GroupSalesTable groupSalesTable) {
		return service.update(groupSalesTable);
	}

	@GetMapping("/{id}")
	public ResponseEntity<GroupSalesTable> getById(@PathVariable String id) {
		return service.getById(id);
	}

	@GetMapping("/")
	public ResponseEntity<List<GroupSalesTable>> getAll() {
		return service.getAll();
	}

	@DeleteMapping("/{id}")
	public Boolean deleteById(@PathVariable String id) {
		return service.deleteById(id);
	}

	@GetMapping("/findByName/{name}")
	public ResponseEntity<GroupSalesTable> findByName(@RequestParam String name) {
		return service.findByName(name);
	}
}
