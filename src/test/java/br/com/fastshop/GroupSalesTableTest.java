package br.com.fastshop;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import br.com.fastshop.entity.GroupSalesTable;
import br.com.fastshop.service.GroupSalesTableService;

@RunWith(MockitoJUnitRunner.class)
public class GroupSalesTableTest {

	@Mock
	private GroupSalesTableService service;

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testMockCreation() {
		assertNotNull(service);
	}

	@Test
	public void testCreate() {
		GroupSalesTable groupSales = new GroupSalesTable();
		when(service.create(groupSales)).thenReturn(ResponseEntity.ok(groupSales));
		assertEquals(ResponseEntity.ok(groupSales), service.create(groupSales));
	}

	@Test
	public void testUpdate() {
		GroupSalesTable groupSales = new GroupSalesTable();
		when(service.update(groupSales)).thenReturn(ResponseEntity.ok(groupSales));
		assertEquals(ResponseEntity.ok(groupSales), service.update(groupSales));
	}

	@Test
	public void testGetdById() {
		GroupSalesTable groupSales = new GroupSalesTable();
		when(service.getById("A123")).thenReturn(ResponseEntity.ok(groupSales));
		assertEquals(ResponseEntity.ok(groupSales), service.getById("A123"));
	}

}
